# 牛哥的小程序组件库 v1.0.3

 [查看代码源：https://gitee.com/HnaQian/niu-xcx-ci](https://gitee.com/HnaQian/niu-xcx-ci)

## 安装

```bash
npm install -g niu_xcx_ci@latest
```

## 示例

```bash
$ niu
Usage: niu [options] [command]

Options:
  -V, --version        output the version number
  -h, --help           display help for command

Commands:
  init <projectName>   项目初始化
  add <componentName>  添加组件
  help [command]       display help for command
```

## 更新日志

- 2022-03-24 v1.0.3 

> 添加使用文档

## 组件库

### 1、QActionSheet 弹窗组件

该组件作为底层基础组件，可以直接在页面中使用实现简单的弹窗，也可在此基础之上编写一定业务的自定义组件

| 属性       | 类型          | 默认值 | 必填 | 说明                 |
| ---------- | ------------- | ------ | ---- | -------------------- |
| isShow     | Boolean       | False  | 是   | 是否展示弹窗         |
| actionType | BOTTOM/CENTER | CENTER | 否   | 展示位置             |
| iscloosebg | Boolean       | true   | 否   | 点击阴影是否关闭弹窗 |
| zIndex     | Number        | 998    | 否   | 弹窗层级             |
| closeModal | eventhandle   |        | 否   | 当关闭弹窗时         |

actionType=BOTTOM的示例：

```json
{
  "usingComponents": {
    "QActionSheet": "/components/QActionSheet/index"
  }
}
```

```html

<QActionSheet
  isShow="{{isShowTan}}"
  actionType="BOTTOM"
>
  <view class="test" slot="actionContent">
  </view>
</QActionSheet>
```



<img src="./ex/IMG_2333.png" style="zoom:75%" />

### 2、QButton 按钮

| 属性  | 类型    | 默认值 | 必填 | 说明                     |
| ----- | ------- | ------ | ---- | ------------------------ |
| width | Number  | 350    | 否   | 宽度（rpx）              |
| plain | boolean | false  | 否   | 按钮是否镂空，背景色透明 |

```json
{
  "usingComponents": {
    "QButton": "/components/QButton/index"
  }
}
```

```html
<QButton width="{{686}}">确认</QButton>
```



### 3、QFormTCI 表单输入组合

包含标题、内容和一个图片/视频

| 属性     | 类型        | 默认值 | 必填 | 说明                           |
| -------- | ----------- | ------ | ---- | ------------------------------ |
| formdata | eventhandle |        | 否   | 当数据发生变化的时候都会被调用 |

```json
{
  "usingComponents": {
    "QFormTCI": "/components/QFormTCI/index"
  }
}
```

```html
<QFormTCI catch:formdata="formdata"></QFormTCI>
```

![IMG_2334](./ex/IMG_2334.png)

### 4、QKeyValueList 键值展示列表

| 属性 | 类型          | 默认值 | 必填 | 说明 |
| ---- | ------------- | ------ | ---- | ---- |
| list | Array<object> | []     | 是   | 数据 |

list：

| 属性  | 类型   | 说明                                     |
| ----- | ------ | ---------------------------------------- |
| key   | string | 左边文案                                 |
| value | string | 右边文案                                 |
| url   | string | 如果不为空，会出现向右箭头，点击可以跳转 |



```json
{
  "usingComponents": {
    "QKeyValueList": "/components/QKeyValueList/index"
  }
}
```

```html
<QKeyValueList list="{{uinfo}}" />
```



<img src="./ex/image-20220324153543922.png" alt="image-20220324153543922" style="zoom:50%;" />



### 5、QModal modal弹窗（依赖组件QActionSheet）

| 属性       | 类型        | 默认值 | 必填 | 说明                 |
| ---------- | ----------- | ------ | ---- | -------------------- |
| isShow     | Boolean     | False  | 是   | 是否展示弹窗数据     |
| title      | string      |        | 是   | 标题                 |
| iscloosebg | Boolean     | true   | 否   | 点击阴影是否关闭弹窗 |
| leftBtn    | eventhandle |        | 否   | 确定                 |
| rightBtn   | eventhandle |        | 否   | 取消                 |



```json
{
  "usingComponents": {
    "QModal": "/components/QModal/index"
  }
}
```

```html
<QModal isShow="{{show}}" title="确定要出去么？" catch:leftBtn="leftBtn" catch:rightBtn="rightBtn" />
```



### 6、QNavigationBar 导航栏

| 属性         | 类型         | 默认值             | 必填 | 说明                       |
| ------------ | ------------ | ------------------ | ---- | -------------------------- |
| homeUrl      | String       | /pages/index/index | 否   | 是否展示弹窗数据           |
| mode         | BLACK、WHITE | BLACK              | 否   | 文字模式（白色、黑色）     |
| title        | String       |                    | 否   | 标题                       |
| customStyle  | String       |                    | 否   | 自定义背景样式             |
| leftIconType | BACK、EMPTY  | BACK               | 否   | 左边icon类型（返回，空白） |

### 7、QPage 页面基础组件 （依赖组件QNavigationBar）

| 属性      | 类型   | 默认值 | 必填 | 说明                                                         |
| --------- | ------ | ------ | ---- | ------------------------------------------------------------ |
| navigator | object | null   | 否   | 导航栏属性见QNavigationBar的属性，如果为null，则不展示导航栏 |

### 8、QSwiper 轮播图

| 属性       | 类型          | 默认值 | 必填 | 说明        |
| ---------- | ------------- | ------ | ---- | ----------- |
| dataSource | Array<object> | []     | 是   | 数据源      |
| height     | number        | 300    | 否   | 高度（rpx） |

datasource：

| 属性 | 类型   | 说明                     |
| ---- | ------ | ------------------------ |
| img  | string | 图片链接                 |
| url  | string | 如果为空，点击不可以跳转 |



### 9、QTabs 

| 属性          | 类型                  | 默认值 | 必填 | 说明                 |
| ------------- | --------------------- | ------ | ---- | -------------------- |
| tabList       | array\<{text:string}> |        | 是   | tab数据              |
| current       | Number                | 0      | 否   | 默认第几个高亮       |
| type          | CENTER、LEFT          | CENTER | 否   | 展示类型，居中、居左 |
| changeCurrent | eventhandle           |        | 否   | 切换tab的时候        |



### 10、QTagChoose 标签选择

| 属性      | 类型            | 默认值 | 必填 | 说明       |
| --------- | --------------- | ------ | ---- | ---------- |
| title     | string          | 标题   | 是   | 标题       |
| tags      | array《string》 | []     | 否   | 选项       |
| tagChange | eventhandle     | CENTER | 否   | 点击了选项 |

![11881648109179_.pic](./ex/11881648109179_.png)



