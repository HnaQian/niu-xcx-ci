

//Component Object
Component({
  properties: {
    isShow: {
      type: Boolean,
      value: false,
    },
    iscloosebg: {
      type: Boolean,
      value: false,
    },
    title: {
      type: String,
      value: '',
    }

  },
  data: {

  },
  methods: {
    leftBtn() {
      this.triggerEvent('leftBtn');
    },
    rightBtn() {
      this.triggerEvent('rightBtn');
    },

  },
});