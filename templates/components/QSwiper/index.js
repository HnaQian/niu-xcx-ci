


//Component Object
Component({
  properties: {
    /**
     * 数据源类型
     * [
     *   {
     *     img: string, 图片链接
     *     url: string, 跳转链接  
     *   }
     * ]
     */
    dataSource:{
      type: Array,
      value: [],
      observer: function(){}
    },
    height: {
      type: Number,
      value: 300
    }
  },
  data: {
    current: 0,
  },
  methods: {
    swiperChange(e) {
      const {
        current,
      } = e.detail;
      this.setData({
        current,
      });
    },
    onClickImg(e) {
      console.log(e)
      const { item } = e.currentTarget.dataset
      if (item && item.url) {
        wx.navigateTo({
          url: item.url,
        });
      }
    }
  },

  attached() {

  },

});