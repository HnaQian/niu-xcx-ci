


//Component Object
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    width: {
      type: Number,
      value: 350,
    },
    plain: {
      type: Boolean,
      value: false
    }

  },
  data: {

  },
  observers: {

  },
  lifetimes: {
    attached() {

    }
  },
  methods: {
    
  },

  

});