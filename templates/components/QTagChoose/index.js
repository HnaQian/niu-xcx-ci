


//Component Object
Component({
  properties: {
    title: {
      type: String,
      value: '标题'
    },
    tags:{
      type: Array,
      value: ['维修', '维修', '维修'],
    },

  },
  data: {
    current: -1,
  },
  observers: {

  },
  lifetimes: {
    attached() {

    }
  },
  methods: {
    onclickTag(e) {
      let {index} = e.currentTarget.dataset
      if (this.data.current == index) {
        index = -1
      }
      this.setData({
        current: index
      })
      this.triggerEvent('tagChange', {
        position: index
      });
    }
  },
});