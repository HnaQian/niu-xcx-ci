/* Tabs.js */
const app = getApp()
Component({
  properties: {
    tabList: {
      type: Array,
      value: []
    },
    current:{
      type: Number,
      value: 0
    }, 
    type:{
      type: String,
      value: 'CENTER', // LEFT
    }, 
  },
  data: {
    statusBarHeight: 0,
  },
  attached() {
    
  },
  methods:{
    changeCurrent (e) {
      if (this.data.current == e.currentTarget.dataset.current) return
      this.setData({current: e.currentTarget.dataset.current})
      this.triggerEvent('changeCurrent', {current: e.currentTarget.dataset.current})
    }
  }
})
