


Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    navigator: {
      type: Object,
      value: null,
    }
  },
  data: {
    
  },
  attached() {
    wx.getSystemInfo({
      success: (res) => {
        this.setData({
          statusH: res.statusBarHeight
        })
      }
    })
  },
  methods: {
    
  }
});