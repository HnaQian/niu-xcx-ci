Component({
  /**
   * @param {String} [homeUrl] 跳转首页的地址
   */
  properties: {
    homeUrl: {
      type: String,
      value: '/pages/index/index',
    },
    mode: {
      type: String,
      value: 'BLACK', // 'WHITE'
    },
    title: {
      type: String,
      value: ''
    },
    customStyle: {
      type: String,
      value: ''
    },
    leftIconType: {
      type: String,
      value: 'BACK', // 'EMPTY'
    }

  },
  data: {
    statusH: 20,
    isShowBack: true,
    leftIcon: './back_black.png'
  },
  observers: {
    mode(data) {
      if (data === 'WHITE') {
        this.setData({
          leftIcon: './back_white.png'
        });
        wx.setNavigationBarColor({
          backgroundColor:'#000000',
          frontColor:'#ffffff',
        });
      }
    }
  },
  lifetimes: {
    attached() {
      wx.getSystemInfo({
        success: (res) => {
          this.setData({
            statusH: res.statusBarHeight
          })
        }
      })
    }
  },


  /**
   * 组件的方法列表
   */
  methods: {
    tapBack() {
      if (getCurrentPages().length > 1) {
        wx.navigateBack({ delta: 1 });
      } else {
        wx.reLaunch({ url: this.data.homeUrl });
      }
    }
  }
});
