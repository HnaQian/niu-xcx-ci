const app = getApp()
Component({
    options: {
        multipleSlots: true // 在组件定义时的选项中启用多slot支持
    },
    properties: {
        title: { // 标题名
            type: String,
            value: ''
        },
        rightItem: {
            type: String | Array,
            value: ''
        },
        leftText: {
            type: String,
            value: ''
        },
        parentRoute: {
            type: String,
            value: ''
        },
        parentType: {
            type: String,
            value: 'redirectTo'
        },
        bgColor: {
            type: String,
            value: '#ffffff'
        },
        color: {
            type: String,
            value: '#333'
        },
    },
    data: {
        // 导入图片域名
        imageHost: app.globalData.imageHost,
        statusBarHeight: 0
    },
    attached() {
        wx.getSystemInfo({
            success: (res) => {
                this.setData({
                    statusBarHeight: res.statusBarHeight
                })
            }
        })
    },
    methods: {
        changeCurrent(e) {
            this.setData({ current: e.currentTarget.dataset.current })
        },
        rightBtn() {

            if (this.properties.rightItem === 'back') {
                var pages = getCurrentPages();
                //获取上一个页面的所有的方法和data中的数据
                var lastpage = pages[pages.length - 2]

                if (lastpage) {
                    wx.navigateBack()
                } else {
                    if (this.properties.parentRoute != '') {
                        if (this.properties.parentType == 'redirect') {
                            wx.redirectTo({
                                url: this.properties.parentRoute
                            })
                        } else if (this.properties.parentType == 'navigate') {
                            wx.navigateTo({
                                url: this.properties.parentRoute
                            })
                        }  else if (this.properties.parentType == 'switch') {
                            wx.switchTab({
                                url: this.properties.parentRoute
                            })
                        } 
                    } else {
                        wx.switchTab({
                            url: '/pages/index/index'
                        })
                    }
                }
            } else {

            }
        }
    },

})