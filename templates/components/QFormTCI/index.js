


//Component Object
Component({
  properties: {
    myProperty:{
      type:String,
      value:'',
      observer: function(){}
    },

  },
  data: {
    title: '',
    content: '',
    showurl: '',
    medium: null,
  },
  observers: {

  },
  lifetimes: {
    attached() {

    }
  },
  methods: {
    titlebindinput(e) {
      this.setData({
        title: e.detail.value
      }, () => {
        this.triggerEvent('formdata', {
          title: this.data.title,
          content: this.data.content,
          medium: this.data.medium
        });
      })
    },
    contentbindinput(e) {
      this.setData({
        content: e.detail.value
      }, () => {
        this.triggerEvent('formdata', {
          title: this.data.title,
          content: this.data.content,
          medium: this.data.medium
        });
      })
    },
    chooseMedium() {
      wx.chooseMedia({
        count: 1,
        mediaType: ['image','video'],
        success: (res) => {
          this.setData({
            showurl: res.tempFiles[0].thumbTempFilePath || res.tempFiles[0].tempFilePath,
            medium: {
              type: res.tempFiles[0].thumbTempFilePath ? 'video' : 'image',
              url: res.tempFiles[0].tempFilePath
            }
          }, () => {
            this.triggerEvent('formdata', {
              title: this.data.title,
              content: this.data.content,
              medium: this.data.medium
            });
          })
        }
      })
    },
  },

  

});