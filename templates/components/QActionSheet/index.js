/**
 * 弹窗组件
 *
 * 该组件作为底层基础组件，可以直接在页面中使用实现简单的弹窗，也可在此基础之上编写一定业务的自定义组件
 *
 * @组件样式 (CENTER)
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。############ 。。。。
 * 。。。。############ 。。。。
 * 。。。。############ 。。。。
 * 。。。。############ <-------只需要编写此处代码
 * 。。。。############ 。。。。
 * 。。。。############ 。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 *
 * @组件样式 (BOTTOM)
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * 。。。。。。。。。。。。。。。。
 * ##########################
 * ##########################
 * ##########################   <---只需要编写此处代码
 * ##########################
 * ##########################
 * ##########################
 * ##########################
 * ##########################
 * ##########################
 *
 *
 *
 *
 * @参数1 isShow 是否展示弹窗
 *
 * @参数2 type 类型  枚举（'BOTTOM' | 'CENTER'）默认中间
 *
 * @参数3 iscloosebg 点击隐形区域是否关闭弹窗 默认true
 *
 * @参数4 zIndex 等级 默认998
 *
 * @参数5 isDisabledScroll 防止滚动穿透（如果开启，页面需要滚动则需要使用scroll-view）
 *
 * @回调方法 closeModal 当关闭弹窗时调用
 *
 *
 */

Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    isShow: {
      type: Boolean,
      value: false,
      observer(nval, oval) {
        if (nval) {
          this.setData({
            remove: false
          });
        } else {
          this.closeModalFunc();
        }
      }
    },
    actionType: {
      type: 'BOTTOM' | 'CENTER',
      value: 'CENTER'
    },
    iscloosebg: {
      type: Boolean,
      value: true
    },
    zIndex: String,
    isDisabledScroll: Boolean
  },
  data: {
    remove: false
  },
  attached() {
    this.setData({
      remove: !this.data.isShow
    });
  },
  methods: {
    //关闭弹窗
    closeModal() {
      if (!this.properties.iscloosebg) return;
      this.closeModalFunc();


    },
    closeModalFunc() {
      this.setData({
        isShow: false
      });
      setTimeout(() => {
        this.animationend();
      }, 250);
      this.triggerEvent('closeModal');

    },

    //弹窗动画结束
    animationend() {
      this.setData({
        remove: !this.data.isShow
      });
    },
    canceltouchmove() {
      if (this.data.isDisabledScroll) {
        return;
      }
    }
  }
});