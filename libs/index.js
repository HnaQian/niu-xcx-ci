"use strict";

var _require = require('commander'),
    Command = _require.Command;

var program = new Command();

var init = require('./commands/init');

var add = require('./commands/add'); // const commander = require('commander')


var pageInfo = require('../package.json');

program.version(pageInfo.version);
program.command('init').description('项目初始化').argument('<projectName>').action(function (projectName) {
  init(projectName);
});
program.command('add').description('添加组件').argument('<componentName>').action(function (componentName) {
  add(componentName);
});
program.parse(process.argv);