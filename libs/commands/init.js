"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _path = _interopRequireDefault(require("path"));

var _glob = _interopRequireDefault(require("glob"));

var _ora = _interopRequireDefault(require("ora"));

var _fsExtra = _interopRequireDefault(require("fs-extra"));

var _dir = require("./../utils/dir");

require("colors");

var spinner = null;

module.exports = function (name) {
  spinner = (0, _ora["default"])('正初始化项目').start(); // 检测当前项目是否需要初始化

  if (isNeedInitProject(name)) {
    initProject(name);
  }
};
/**
 * 检测当前项目是否需要进行初始化
 * @param {string} projectDir 开发项目工作根目录
 * @returns 是否需要初始化
 */


function isNeedInitProject(name) {
  console.log(_dir.projectDir); // 获取模版目录文件

  var sourceFiles = _glob["default"].sync("".concat(_dir.projectDir, "/*"));

  sourceFiles.forEach(function (p) {
    if (_path["default"].basename(p) === name) {
      spinner.fail("\u9879\u76EE\u540D\u5DF2\u7ECF\u5B58\u5728\uFF0C\u4FEE\u6539\u9879\u76EE\u540D".red);
      return false;
    }
  });
  return true;
}
/**
 * 初始化项目
 * @param {string} projectDir 开发项目工作根目录
 */


function initProject(_x) {
  return _initProject.apply(this, arguments);
}

function _initProject() {
  _initProject = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(name) {
    var templateDir;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            // 获取项目模版
            templateDir = _path["default"].resolve(__dirname, "../../templates/project");

            _fsExtra["default"].copy(templateDir, _path["default"].join(_dir.projectDir, name), function (err) {
              if (err) return console.error(err);
              spinner.succeed("\u9879\u76EE\u521D\u59CB\u5316\u5B8C\u6210".green);
            }); //拷贝目录


          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _initProject.apply(this, arguments);
}