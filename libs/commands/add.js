"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _path = _interopRequireDefault(require("path"));

var _glob = _interopRequireDefault(require("glob"));

var _ora = _interopRequireDefault(require("ora"));

var _fsExtra = _interopRequireDefault(require("fs-extra"));

require("colors");

var spinner;

module.exports = function (componentName) {
  spinner = (0, _ora["default"])('正初始化组件-' + componentName).start();

  if (isHaveComponent(componentName) && isNeedInitComponent(componentName)) {
    var templateDir = _path["default"].resolve(__dirname, "../../templates/components", componentName);

    _fsExtra["default"].copy(templateDir, _path["default"].join('./src/components', componentName), function (err) {
      if (err) {
        spinner.fail("\u7EC4\u4EF6\u521D\u59CB\u5316\u5931\u8D25".red);
        return console.error(err);
      } else {
        return spinner.succeed("\u7EC4\u4EF6\u521D\u59CB\u5316\u5B8C\u6210".green);
      }
    }); //拷贝目录

  } else {
    return;
  }
};

function isHaveComponent(componentName) {
  var mmm = false;

  var templateDir = _path["default"].resolve(__dirname, "../../templates/components"); // 获取模版目录文件


  var temFiles = _glob["default"].sync("".concat(templateDir, "/*"));

  temFiles.forEach(function (p) {
    if (_path["default"].basename(p) === componentName) {
      mmm = true;
    }
  });

  if (!mmm) {
    spinner.fail("\u672A\u627E\u5230\u7EC4\u4EF6-".concat(componentName).red);
  }

  return mmm;
}
/**
 * 检测当前组件是否需要进行初始化
 * @param {string} projectDir 开发项目工作根目录
 * @returns 是否需要初始化
 */


function isNeedInitComponent(componentName) {
  var mmm = true;

  var sourceFiles = _glob["default"].sync("./src/components/*");

  sourceFiles.forEach(function (p) {
    if (_path["default"].basename(p) === componentName) {
      spinner.fail("\u7EC4\u4EF6-".concat(componentName, "-\u5DF2\u7ECF\u5B58\u5728\uFF0C\u8BF7\u4FEE\u6539\u9879\u76EE\u540D").red);
      mmm = false;
    }
  });
  return mmm;
}