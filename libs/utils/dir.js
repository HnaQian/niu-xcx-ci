"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.projectDir = exports.ROOT_PATH = void 0;

var _path = _interopRequireDefault(require("path"));

// 当前项目的根目录
var ROOT_PATH = process.env.ROOT_PATH || ''; // 项目根目录 (当前执行命令的目录)

exports.ROOT_PATH = ROOT_PATH;

var projectDir = _path["default"].resolve(process.cwd(), ROOT_PATH);

exports.projectDir = projectDir;