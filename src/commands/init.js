import path from 'path'
import glob from 'glob'
import ora from 'ora'
import fs from 'fs-extra'
import { projectDir } from './../utils/dir'
import 'colors'

let spinner = null

module.exports = (name) => {
  spinner = ora('正初始化项目').start();
  // 检测当前项目是否需要初始化
  if (isNeedInitProject(name)){
    initProject(name);
  }
}

/**
 * 检测当前项目是否需要进行初始化
 * @param {string} projectDir 开发项目工作根目录
 * @returns 是否需要初始化
 */
function isNeedInitProject(name) {
  console.log(projectDir)
  // 获取模版目录文件
  const sourceFiles = glob.sync(`${projectDir}/*`)

  sourceFiles.forEach(p => {
    if (path.basename(p) === name) {
      spinner.fail(`项目名已经存在，修改项目名`.red)
      return false
    }
  })
  return true
}

/**
 * 初始化项目
 * @param {string} projectDir 开发项目工作根目录
 */
 async function initProject(name){
  // 获取项目模版
  
  const templateDir = path.resolve(__dirname, `../../templates/project`)
  fs.copy(templateDir, path.join(projectDir, name), function (err) {
    if (err) return console.error(err) 
    spinner.succeed(`项目初始化完成`.green);
  }) //拷贝目录
}