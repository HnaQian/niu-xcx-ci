const { Command } = require('commander');
const program = new Command();
const init = require('./commands/init')
const add = require('./commands/add')

// const commander = require('commander')

const pageInfo = require('../package.json')

program
    .version(pageInfo.version)

program
    .command('init')
    .description('项目初始化')
    .argument('<projectName>')
    .action((projectName) => {
      init(projectName)
    })
program
    .command('add')
    .description('添加组件')
    .argument('<componentName>')
    .action((componentName) => {
      add(componentName)
    })

program.parse(process.argv)