

import path from 'path'


// 当前项目的根目录
export const ROOT_PATH   = process.env.ROOT_PATH || '';
// 项目根目录 (当前执行命令的目录)
export const projectDir  = path.resolve(process.cwd(), ROOT_PATH);